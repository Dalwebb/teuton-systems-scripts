#!/bin/bash

echo "Moving touchpad-fix.sh to /etc/touchpad-fix.sh"
mv ./touchpad-fix.sh /etc/touchpad-fix.sh
echo "Moving touchpad-fix.service to /etc/systemd/system/touchpad-fix.service"
mv ./touchpad-fix.service /etc/systemd/system/touchpad-fix.service
echo "Enabling touchpad-fix.service"
systemctl enable touchpad-fix.service
